#
# Needed variables
## Global (can be overridden at lower-level)
# - vm_number - mandatory !!!
# - create_backup (default: false) - recommended to set "true" on live's deploy job.
# - dist_path (default: dist)
# - drupal_path (default: web) - recommended to use "".
# - drush (default: drush)
# - features_enabled (default: false)
# - ssh_host (default: vms.hosting.1xinternet.de)
# - ssh_port (default: 22)
# - vr_path (default: tests/backstopjs)
#
## Deployment
# - env (live / stage / test) - mandatory
# - htdocs_path - only necessary if not trivially derived from env, must be set as absolute path.
# - ssh_user - only necessary if not identical to env.
#
## Copy database
# - db_source - source database, mandatory !!!
# - db_target - target database, mandatory !!!
# - ssh_user - only necessary if not identical to db_target
# - htdocs_path (default: /var/www/vhosts/${db_target}/htdocs)
# - remove_old_backups (default: true)
#
###################################
#
# Example:
#
# include:
#   - project: '1xInternet/gitlab-ci-common'
#     file: 'gitlab-ci-d7.yml'
#
# variables:
#   vm_number: nnn
#   drupal_path: ""
#   features_enabled: "true"
#   ssh_port: 10$vm_number
#
# deploy_to_prod:
#   stage : deploy
#   extends: .drupal7_deploy
#   variables:
#     env: live
#     create_backup: "true"
#   environment:
#     url: https://example.com
#   before_script:
#     - echo "commands to execute before the common deploy script runs"
#   after_script:
#     - echo "commands to execute after the common deploy script runs"
#   only:
#     - main
#
# copy_database:
#   stage: build
#   extends: .drupal7_copy_database

variables:
  create_backup: "false"
  dist_path: dist
  drupal_path: web
  drush: drush
  features_enabled: "false"
  ssh_host: vms.hosting.1xinternet.de
  ssh_port: 22
  vr_path: tests/backstopjs

.docker_drupal7_deploy:
  extends: .drupal7_deploy
  before_script:
    - apk add openssh-client
    - mkdir ~/.ssh && cp $SSH_KEY_1X ~/.ssh/id_rsa && chmod 400 ~/.ssh/id_rsa
    - echo "StrictHostKeyChecking no" >> ~/.ssh/config
  variables:
    GIT_STRATEGY: none

.drupal7_deploy:
  variables:
    htdocs_path: /var/www/vhosts/${env}/htdocs
    ssh_user: ${env}
  environment:
    name: ${env}
    url: http://${env}.vm${vm_number}.hosting.1xinternet.de
  script:
    - echo '--- Uploading Artifact' && scp -P ${ssh_port} archive.tar ${ssh_user}@${ssh_host}:${htdocs_path}/gitlab-${CI_JOB_ID}-${CI_COMMIT_SHA}.tar
    - |
      ssh -T -A -p ${ssh_port} ${ssh_user}@${ssh_host} << DEPLOY
        set -e
        /usr/bin/env php -v
        pushd ${htdocs_path}/${dist_path}/${drupal_path} > /dev/null && ${drush} sqlq --extra=--skip-column-names "SELECT VERSION();" && popd > /dev/null
        echo '--- Extracting Artifact'
        pushd ${htdocs_path} > /dev/null
        mkdir -p gitlab-${CI_JOB_ID}-${CI_COMMIT_SHA}
        tar poxf gitlab-${CI_JOB_ID}-${CI_COMMIT_SHA}.tar -C gitlab-${CI_JOB_ID}-${CI_COMMIT_SHA}
        rm gitlab-${CI_JOB_ID}-${CI_COMMIT_SHA}.tar
        popd > /dev/null
        if [ -f ${htdocs_path}/${dist_path}/${drupal_path}/index.php ]; then
          pushd ${htdocs_path}/${dist_path}/${drupal_path} > /dev/null
          echo '--- Activate Maintenancemode'
          ${drush} vset maintenance_mode 1
          if [[ "${create_backup}" == "true" ]]; then
            echo '--- Create DB backup'
            mkdir -p ${htdocs_path}/db_backups
            ${drush} sql-dump --structure-tables-list=cache,cache_*,history,sessions,watchdog > ${htdocs_path}/db_backups/gitlab-${CI_JOB_ID}-${CI_COMMIT_SHA}.sql
            gzip -9 ${htdocs_path}/db_backups/gitlab-${CI_JOB_ID}-${CI_COMMIT_SHA}.sql;
          fi
          popd > /dev/null;
        fi
        echo '--- Creating Symlinks'
        pushd ${htdocs_path}/gitlab-${CI_JOB_ID}-${CI_COMMIT_SHA}/${drupal_path} > /dev/null
        pushd sites/default > /dev/null
        if [ -d files ]; then
          rmdir files | true;
        fi
        if [ -d private ]; then
          rmdir private | true;
        fi
        if [ ! -e settings.php ]; then
          ln -s ${htdocs_path}/settings.php;
        else
          if [ -e ${htdocs_path}/settings.local.php ]; then
            ln -s ${htdocs_path}/settings.local.php;
          fi;
        fi
        ln -s ${htdocs_path}/files
        ln -s ${htdocs_path}/private
        popd > /dev/null
        pushd ${htdocs_path}/gitlab-${CI_JOB_ID}-${CI_COMMIT_SHA}
        if [ -x scripts/before-deploy.sh ]
        then
          echo '--- Executing commands before deployment'
          ./scripts/before-deploy.sh
        fi
        popd
        echo '--- Archiving old artifact'
        pushd ${htdocs_path} > /dev/null
        olddist=\$(readlink -n ${htdocs_path}/${dist_path})
        mv \${olddist} \${olddist}.archived
        rm ${dist_path} | true
        ln -s \${olddist}.archived ${dist_path}
        popd > /dev/null
        echo '--- Clearing APCu cache'
        php -r 'if (function_exists('apcu_clear_cache')) { echo "APC-User cache: " . apcu_clear_cache() . "\n"; }'
        ${drush} cc all
        if [ -z \$(${drush} status drupal-version --format=list) ]; then
          echo '!!! Drupal does not seem to be installed. Cleaning up and exiting.'
          pushd ${htdocs_path} > /dev/null
          mv \${olddist}.archived \${olddist}
          rm ${dist_path} | true
          ln -s \${olddist} ${dist_path}
          popd > /dev/null
          pushd ${htdocs_path}/${dist_path}/${drupal_path} > /dev/null
          ${drush} vset maintenance_mode 0
          exit 1;
        else
          echo '--- Drupal core is alive!';
        fi
        popd > /dev/null
        echo '--- Activate New Artifact'
        pushd ${htdocs_path} > /dev/null
        rm ${dist_path} | true
        ln -s gitlab-${CI_JOB_ID}-${CI_COMMIT_SHA} ${dist_path}
        touch gitlab-${CI_JOB_ID}-${CI_COMMIT_SHA}
        pushd ${dist_path}/${drupal_path} > /dev/null
        ${drush} updb -y
        if [[ "${features_enabled}" == "true" ]]; then
          ${drush} fra -y;
        fi
        popd > /dev/null
        popd > /dev/null
        pushd ${htdocs_path}/${dist_path}
        if [ -x scripts/after-deploy.sh ]
        then
          echo '--- Executing commands after deployment'
          ./scripts/after-deploy.sh
        fi
        popd
        echo '--- Deactivate Maintenancemode'
        pushd ${htdocs_path}/${dist_path}/${drupal_path} > /dev/null
        ${drush} vset maintenance_mode 0
        popd > /dev/null
        echo '--- Delete old Artifacts'
        pushd ${htdocs_path} > /dev/null
        for i in \$(ls -1tdr gitlab-*|sed -n -e :a -e '1,3!{P;N;D;};N;ba'); do
          chmod -R u+w \$i;
          rm -fr \$i;
        done
        popd > /dev/null
        if [[ "${create_backup}" == "true" ]]; then
          pushd ${htdocs_path}/db_backups > /dev/null
          for i in \$(ls -1tdr gitlab-*.sql.gz|sed -n -e :a -e '1,2!{P;N;D;};N;ba'); do
            rm -f \$i;
          done
          popd > /dev/null;
        fi
      DEPLOY
  except:
    - schedules

.drupal7_security_updates:
  allow_failure: true
  script:
    - echo '--- CI-based security updates have been deprecated, and replaced with a Tugboat-based alternative. Please remove the security update rule from the .gitlab-ci.yml file'

.drupal7_vr_reference:
  allow_failure: true
  script:
    - echo '--- CI-based VR tests have been deprecated, and replaced with a Tugboat-based alternative. Please remove the VR test rules from the .gitlab-ci.yml file'

.drupal7_vr_test:
  allow_failure: true
  script:
    - echo '--- CI-based VR tests have been deprecated, and replaced with a Tugboat-based alternative. Please remove the VR test rules from the .gitlab-ci.yml file'

.docker_drupal7_copy_database:
  extends: .drupal7_copy_database
  before_script:
    - apk add openssh-client
    - mkdir ~/.ssh && cp $SSH_KEY_1X ~/.ssh/id_rsa && chmod 400 ~/.ssh/id_rsa
    - echo "StrictHostKeyChecking no" >> ~/.ssh/config
  variables:
    GIT_STRATEGY: none

.drupal7_copy_database:
  variables:
    # copy_database variables
    remove_old_backups: "true"
    # source remote variables
    ssh_port_source: ${ssh_port}
    ssh_user_source: ${db_source}
    ssh_host_source: ${ssh_host}
    htdocs_path_source: /var/www/vhosts/${db_source}/htdocs
    dist_path_source: ${dist_path}
    # target remote variables
    ssh_port_target: ${ssh_port}
    ssh_user_target: ${db_target}
    ssh_host_target: ${ssh_host}
    htdocs_path_target: /var/www/vhosts/${db_target}/htdocs
    dist_path_target: ${dist_path}
  script:
    - |
      if [[ "${db_target}" == "live" ]]; then
        echo "Aborting attempt to overwrite live DB"
        exit 1
      fi
    - |
      if [[ -z ${db_source} || -z ${db_target} ]]; then
        echo "Oops... One or more variables are undefined"
        exit 1
      fi
    - |
      echo "------ Logged in the source environment (${db_source}) to create a db dump"
      ssh -T -A -o StrictHostKeyChecking=no -p ${ssh_port_source} ${ssh_user_source}@${ssh_host_source} << COPYDB_SOURCE
        set -e
        pushd ${htdocs_path_source}/${dist_path_source}/${drupal_path} > /dev/null
        echo "--- Dumping the current source database"
        ${drush} sql-dump > ${htdocs_path_source}/copy_database-${CI_JOB_ID}-${CI_COMMIT_SHA}.sql
        gzip -9 ${htdocs_path_source}/copy_database-${CI_JOB_ID}-${CI_COMMIT_SHA}.sql
        popd > /dev/null
      COPYDB_SOURCE
    - |
      echo "------ Transferring the source database from (${db_source}) to (${db_target}) via the GitLab runner"
      echo "--- Moving the database from (${db_source}) to the GitLab runner"
      scp -P ${ssh_port_source} ${ssh_user_source}@${ssh_host_source}:${htdocs_path_source}/copy_database-${CI_JOB_ID}-${CI_COMMIT_SHA}.sql.gz .
      echo "--- Moving the database from the GitLab runner to (${db_target})"
      scp -P ${ssh_port_target} copy_database-${CI_JOB_ID}-${CI_COMMIT_SHA}.sql.gz ${ssh_user_target}@${ssh_host_target}:${htdocs_path_target}/.
      rm copy_database-${CI_JOB_ID}-${CI_COMMIT_SHA}.sql.gz
    - |
      echo "------ Cleanup on the source environment (${db_source})"
      ssh -T -A -o StrictHostKeyChecking=no -p ${ssh_port_source} ${ssh_user_source}@${ssh_host_source} << CLEAN_SOURCE
        set -e
        rm ${htdocs_path_source}/copy_database-${CI_JOB_ID}-${CI_COMMIT_SHA}.sql.gz
      CLEAN_SOURCE
    - |
      echo "------ Logged in the target environment (${db_target})"
      ssh -T -A -o StrictHostKeyChecking=no -p ${ssh_port_target} ${ssh_user_target}@${ssh_host_target} << COPYDB_TARGET
        set -e
        echo "--- Creating a backup of the current target database"
        mkdir -p ${htdocs_path_target}/db_backups/manual
        pushd ${htdocs_path_target}/${dist_path_target}/${drupal_path} > /dev/null
        ${drush} sql-dump > ${htdocs_path_target}/db_backups/manual/gitlab-${CI_JOB_ID}-${CI_COMMIT_SHA}.sql
        gzip -9 ${htdocs_path_target}/db_backups/manual/gitlab-${CI_JOB_ID}-${CI_COMMIT_SHA}.sql
        popd > /dev/null
        echo "--- Import the source database"
        pushd ${htdocs_path_target}/${dist_path_target}/${drupal_path} > /dev/null
        ${drush} sql-drop -y
        gzip -dc ${htdocs_path_target}/copy_database-${CI_JOB_ID}-${CI_COMMIT_SHA}.sql.gz | ${drush} sqlc
        popd > /dev/null
        rm ${htdocs_path_target}/copy_database-${CI_JOB_ID}-${CI_COMMIT_SHA}.sql.gz
        if [[ "${remove_old_backups}" == "true" ]]; then
          echo "--- Cleanup the older manual backups on the target environment (${db_target})"
          pushd ${htdocs_path_target}/db_backups/manual > /dev/null
          for i in \$(ls -1tdr gitlab-*.sql.gz | sed -n -e :a -e '1,2!{P;N;D;};N;ba'); do
            rm -f \$i;
          done
          popd > /dev/null
        fi
        curl -X POST -F token=${CI_JOB_TOKEN} -F ref=${CI_COMMIT_REF_NAME} https://git.1xinternet.de/api/v4/projects/${CI_PROJECT_ID}/trigger/pipeline
      COPYDB_TARGET
  only:
    refs:
      - schedules
    variables:
      - $SCHEDULE_TYPE == "copy_db"
  except:
    variables:
      - $db_source == ""
      - $db_target == ""

.backup:on-schedule:
  image: git.1xinternet.de:4567/devops/offsite-backups/backup_vm
  script:
    - /bin/bash /usr/src/bcp/backup.sh
  only:
    refs:
      - schedules
    variables:
      - $SCHEDULE_TYPE == "backup"
  allow_failure: false
  tags:
    - backups

.acquia_backup:on-schedule:
  image: git.1xinternet.de:4567/devops/offsite-backups/backup_vm
  script:
    - /bin/bash /usr/src/bcp/acquia_backup.sh
  only:
    refs:
      - schedules
    variables:
      - $SCHEDULE_TYPE == "acquia_backup"
  allow_failure: false
  tags:
    - backups
