# GitLab CI common

This repository holds the common CI scripts used by almost all projects, you can found more information inside code.

Lets see some features:

### Before and after scripts

The scripts are meant to execute commands needed in a specific project before or after it's deployment. The commands are executed at repository root.

This scripts are located at `scripts`. So they will be located on `scripts/before-deploy.sh` and `scripts/after-deploy.sh`. **They need to be executables**

In the case you need to execute some specific commands depending on the environment, you can use the `USER` enviroment variable.

```
#!/bin/sh
# This scripts purpose is to execute the specific commands needed before the deployment of the project in a given enviroment.

# Function for error catching.
# @see http://linuxcommand.org/lc3_wss0140.php
error_exit () {
  echo "$1" 1>&2
  exit 1
}

if [ -n $USER ]
then
    case $USER in
    "test")
        #Commands go here
    ;;
    "stage")
        #Commands go here
    ;;
    "live")
        #Commands go here
    ;;
    *)
        error_exit "Check that the enviroments have the correct names asigned to them."
    ;;
    esac
fi

```
